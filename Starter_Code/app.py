# Import the dependencies.
import numpy as np
import pandas as pd

import sqlalchemy
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine, func

from flask import Flask, jsonify


#################################################
# Database Setup
#################################################
# create engine to hawaii.sqlite
engine = create_engine("sqlite:///Resources/hawaii.sqlite")

# reflect an existing database into a new model
Base = automap_base()
# reflect the tables
Base.prepare(autoload_with=engine)


# Save references to each table
measurement, station = Base.classes

# Create our session (link) from Python to the DB
# session = Session(engine)


#################################################
# Flask Setup
#################################################
app = Flask(__name__)



#################################################
# Flask Routes
#################################################

@app.route("/")
def welcome():
    """List all available api routes."""
    return (
        f"Available Routes:<br/>"
        f"/api/v1.0/precipitation<br/>"
        f"/api/v1.0/stations<br/>"
        f"/api/v1.0/tobs<br/>"
        f"/api/v1.0/&lt;start&gt;<br/>"  
        f"/api/v1.0/&lt;start&gt;/&lt;end&gt;"
    )

@app.route("/api/v1.0/precipitation")
def precipitation():
    session = Session(engine)
    
    # Find the most recent date in the data set.
    recent_date = session.query(measurement).order_by(measurement.date).all()[-1]
    recent_date.__dict__['date']

    # Calculate the date one year from the latest date
    one_year_ago = (pd.to_datetime(recent_date.__dict__['date']) - pd.DateOffset(days=365)).strftime('%Y-%m-%d')
    one_year_ago

    # Perform a query to retrieve the data and precipitation scores
    precipitation_data = session.query(measurement.date, measurement.prcp).\
        filter(measurement.date >= one_year_ago).\
        order_by(measurement.date).all()
    
    # Close Session
    session.close()

    # Convert the results to a dictionary with date as the key and prcp as the value
    precipitation_data = {date: prcp for date, prcp in precipitation_data}


    # Return the JSON representation of the dictionary
    return jsonify(precipitation_data)


@app.route("/api/v1.0/stations")
def stations():
    
    session = Session(engine)
    
    # List the stations and their counts in descending order.
    active_stations = session.query(measurement.station).\
    group_by(measurement.station).\
    order_by(func.count(measurement.station).desc()).all()

    # Close Session
    session.close()

    # Assuming active_stations is the result of your query
    active_stations = [station[0] for station in active_stations]

    # Return the JSON representation of the dictionary
    return jsonify(active_stations)


@app.route("/api/v1.0/tobs")
def tobs():
    
    session = Session(engine)
    
    # Find the most recent date in the data set.
    recent_date = session.query(measurement).order_by(measurement.date).all()[-1]
    recent_date.__dict__['date']

    # Calculate the date one year from the latest date
    one_year_ago = (pd.to_datetime(recent_date.__dict__['date']) - pd.DateOffset(days=365)).strftime('%Y-%m-%d')
    one_year_ago
    
    # List the stations and their counts in descending order.
    active_stations = session.query(measurement.station).\
    group_by(measurement.station).\
    order_by(func.count(measurement.station).desc()).all()

    # Assuming active_stations is the result of your query
    active_stations = [station[0] for station in active_stations]
    
    # Most active station id from the previous query
    most_active_station_id = active_stations[0]

    # Query the last 12 months of temperature observation data for this station and plot the results as a histogram
    temperature_data = session.query(measurement.tobs).\
    filter(measurement.station == most_active_station_id, measurement.date >= one_year_ago).all()
    
    # Convert the result to a list for response
    temperature_list = [temp[0] for temp in temperature_data]
    
    
    # Close Session
    session.close()

    # Return the JSON representation of the dictionary
    return jsonify(temperature_list)


@app.route("/api/v1.0/<start>")
def temperature_stats_start(start):
    """Return TMIN, TAVG, and TMAX for dates greater than or equal to the start date."""
    
    session = Session(engine)
    
    # Query using 'start' parameter
    result = session.query(
        func.min(measurement.tobs).label('TMIN'),
        func.avg(measurement.tobs).label('TAVG'),
        func.max(measurement.tobs).label('TMAX')
    ).filter(measurement.date >= start).first()

    # Convert the result to a dictionary
    temperature_data = {
        "TMIN": result.TMIN,
        "TAVG": result.TAVG,
        "TMAX": result.TMAX
    }
    
    # Close Session
    session.close()

    # Return the JSON response
    return jsonify(temperature_data)

@app.route("/api/v1.0/<start>/<end>")
def temperature_stats_start_end(start, end):
    """Return TMIN, TAVG, and TMAX for dates between the start and end dates (inclusive)."""
    
    session = Session(engine)

    # Query using 'start' and 'end' parameters
    result = session.query(
        func.min(measurement.tobs).label('TMIN'),
        func.avg(measurement.tobs).label('TAVG'),
        func.max(measurement.tobs).label('TMAX')
    ).filter(measurement.date.between(start, end)).first()

    # Convert the result to a dictionary
    temperature_data = {
        "TMIN": result.TMIN,
        "TAVG": result.TAVG,
        "TMAX": result.TMAX
    }
    
    # Close Session
    session.close()
    
    # Return the JSON response
    return jsonify(temperature_data) 
    


if __name__ == '__main__':
    app.run(debug=True)